# Tutorial Website

This is a tutorial on how to use golang as a server with templates, and a few tweaks with CSS and jQuery

Full tutorial can be found [here](https://blog.ignifi.me/codified/2021/02/03/go-and-the-world-wide-web/)
