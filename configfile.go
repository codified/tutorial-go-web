package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"os"
)

type ConfigFile struct {
	Port string `json:"port"`
}

func createConfigFile() ConfigFile {
	p := "./config.json"

	var cf ConfigFile

	cf.Port = "8080"

	j, _ := json.Marshal(cf)

	ioutil.WriteFile(p, j, 0644)

	return cf
}

func readConfigFile() ConfigFile {
	p := "./config.json"

	_, err := os.Stat(p)

	var cf ConfigFile
	if os.IsNotExist(err) {
		log.Println("Could not find config file, creating it")
		cf = createConfigFile()
	} else if err == nil {
		b, _ := ioutil.ReadFile(p)
		json.Unmarshal(b, &cf)
	}

	return cf
}
