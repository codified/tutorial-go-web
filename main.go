package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"text/template"
	"time"

	mux "github.com/gorilla/mux"
)

type mainpageHandler struct {
	staticPath string
	indexPath  string
}

func myinit() ConfigFile {
	cf := readConfigFile()
	return cf
}

func error404Handler(w http.ResponseWriter, r *http.Request, status int) {
	w.WriteHeader(status)
	if status == http.StatusNotFound {
		fmt.Fprint(w, "custom 404")
	}
}

func cook() {
	cookIndex()
	cookLogin()
	cookNewUser()
}

func cookIndex() {
	os.RemoveAll("public")
	os.MkdirAll("public", 0777)

	t, err := template.ParseFiles("templates/index.xhtml", "templates/footer.xhtml",
		"templates/header.xhtml", "templates/menu.xhtml")

	if err != nil {
		log.Fatal(err)
	}

	f, err := os.Open("public/index.html")

	if err != nil {
		f, _ = os.Create("public/index.html")
	}

	err = t.Execute(f, "")

	if err != nil {
		log.Fatal(err)
	}
}

func cookLogin() {
	os.MkdirAll("public/login", 0777)

	t, err := template.ParseFiles("templates/login.xhtml", "templates/footer.xhtml",
		"templates/header.xhtml", "templates/menu.xhtml")

	if err != nil {
		log.Fatal(err)
	}

	f, err := os.Open("public/login/index.html")

	if err != nil {
		f, _ = os.Create("public/login/index.html")
	}

	err = t.Execute(f, "")

	if err != nil {
		log.Fatal(err)
	}
}

func cookNewUser() {
	os.MkdirAll("public/newuser", 0777)

	t, err := template.ParseFiles("templates/newuser.xhtml", "templates/footer.xhtml",
		"templates/header.xhtml", "templates/menu.xhtml")

	if err != nil {
		log.Fatal(err)
	}

	f, err := os.Open("public/newuser/index.html")

	if err != nil {
		f, _ = os.Create("public/newuser/index.html")
	}

	err = t.Execute(f, "")

	if err != nil {
		log.Fatal(err)
	}
}

func (h mainpageHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {

	path, err := filepath.Abs(r.URL.Path)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	path = filepath.Join(h.staticPath, path)

	_, err = os.Stat(path)
	if os.IsNotExist(err) {
		error404Handler(w, r, http.StatusNotFound)
		return
	} else if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	http.FileServer(http.Dir(h.staticPath)).ServeHTTP(w, r)
}

func main() {
	cook()

	cf := myinit()

	r := mux.NewRouter()

	hs := http.FileServer(http.Dir("./resources/"))
	r.PathPrefix("/resources/").Handler(http.StripPrefix("/resources/", hs))

	mainH := mainpageHandler{staticPath: "public", indexPath: "index.html"}
	r.PathPrefix("/").Handler(mainH)

	fmt.Println("Listening on port: " + cf.Port)

	srv := &http.Server{
		Handler: r,
		Addr:    "127.0.0.1:" + cf.Port,

		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}

	log.Fatal(srv.ListenAndServe())
}
